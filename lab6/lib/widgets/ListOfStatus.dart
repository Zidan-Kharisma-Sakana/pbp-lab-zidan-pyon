import 'package:flutter/material.dart';
import '../dummy.dart';
import 'StatusCard.dart';
import '../models/status.dart';

class ListOfStatus extends StatelessWidget {
  List<Status> status;
  ListOfStatus({Key? key,required this.status}) : super(key: key);
  void _test() {}

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
      children: status
          .map((e) => StatusCard(
                id: e.id,
                message: e.message,
                sender: e.sender,
                liker: 2,
              ))
          .toList(),
    ),
    );
  }
}
