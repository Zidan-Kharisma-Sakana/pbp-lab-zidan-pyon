import 'package:flutter/material.dart';

class ProfileCard extends StatelessWidget {
  ProfileCard({Key? key}) : super(key: key);
  void _test(){
    
  }
  final TextStyle headTextStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  final TextStyle fieldStyle = TextStyle(fontSize: 20, fontWeight: FontWeight.w400);
  final TextStyle normalStyle = TextStyle(fontSize: 20);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[Title(color: Colors.brown, child: Text("Profile", style: headTextStyle ,)),
            ElevatedButton(onPressed: _test, child: Text("Edit Profile"))
          ],
        ),
        Row(children: <Widget>[Text("Nama:", style: fieldStyle,), Text("Pomu", style: normalStyle,) ],),
        Row(children: <Widget>[Text("Email:", style: fieldStyle), Text("Pomu@gmail.com", style: normalStyle) ],),
        Row(children: <Widget>[Text("Ulang Tahun:", style: fieldStyle), Text("March 1", style: normalStyle) ],),
        Row(children: <Widget>[Text("Bio:", style: fieldStyle), Text("Hi!", style: normalStyle) ],),
      ],
    );
  }
}
