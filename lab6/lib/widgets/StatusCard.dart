import 'dart:math';

import 'package:flutter/material.dart';
import 'package:lab6/models/status.dart';

class StatusCard extends StatefulWidget {
  int liker;
  String sender;
  String message;
  String id;
  StatusCard(
      {required this.id,
      required this.sender,
      required this.message,
      required this.liker});
  @override
  _StatusCardState createState() => _StatusCardState();
}

class _StatusCardState extends State<StatusCard> {
  late String _sender;
  late int _liker;
  bool isLiked = false;
  @override
  void initState() {
    super.initState();
    this._sender = widget.sender;
    this._liker = widget.liker;
  }

  void _toggleLike(bool isLike) {
    print("Test " + isLike.toString());
    setState(() {
      isLiked = !isLike;
      if (isLike) {
        widget.liker++;
      } else
        widget.liker--;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      child: Row(children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.only(bottom: 8),
              child: Text(
                _sender,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Text(
              widget.message,
              style: TextStyle(
                color: Colors.grey[500],
              ),
            ),
          ],
        ),
      ]),
    );
    String str = "";
    if (_liker > 0) str = _liker.toString() + " menyukai ini";
    Icon ic = Icon(
      Icons.favorite,
      color: Colors.red,
    );
    if (!isLiked) {
      ic = Icon(
        Icons.favorite_border_sharp,
        color: Colors.red,
      );
    }
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.only(bottom: 20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        children: <Widget>[
          titleSection,
          Row(
            children: <Widget>[
              ElevatedButton.icon(
                  onPressed: () {
                    _toggleLike(isLiked);
                  },
                  style: ElevatedButton.styleFrom(
                      primary: Colors.white,
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                      textStyle:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
                  label: Text(''),
                  icon: ic),
              Text(str)
            ],
          )
        ],
      ),
    );
  }
}
