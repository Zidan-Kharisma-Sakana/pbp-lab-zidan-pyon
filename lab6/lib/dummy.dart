import 'models/status.dart';

List<Status> DUMMY_STATUS = <Status>[
  Status(id: "1", message: "Hi", sender: "You-chan"),
  Status(id: "2", message: "Zenshoku zenshin... Yousoro!", sender: "You-chan"),
  Status(id: "1", message: "Hi 2 you!", sender: "You-chan"),
  Status(id: "1", message: "I LOVE RIKO CHAN", sender: "You-chan"),
];
