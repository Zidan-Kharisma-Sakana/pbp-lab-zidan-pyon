import 'package:flutter/material.dart';
import 'widgets/ProfileCard.dart';
import 'widgets/ListOfStatus.dart';
import 'dummy.dart';
import 'widgets/AddStatus.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Be Productive',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Home'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title,
          style: TextStyle(color: Colors.brown),
        ),
        backgroundColor: Colors.yellow,
      ),
      body: Container(
        child:SingleChildScrollView(child:  Column(
          children: <Widget>[
            ProfileCard(),
            SizedBox(
              height: 20,
            ),
            AddStatus(),
            ListOfStatus(status: DUMMY_STATUS)
          ], //buat list of status
        ),),
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
      ),
    );
  }
}
