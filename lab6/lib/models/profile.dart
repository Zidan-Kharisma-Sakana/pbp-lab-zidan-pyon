import 'package:flutter/material.dart';
import './status.dart';
class Profile {
  final String id;
  final String nama;
  final String birthday;
  final String bio;
  List<Status> status = [];
  
  Profile({
    required this.id,
    required this.nama,
    required this.birthday,
    required this.bio
  });

}
