import 'package:flutter/material.dart';

class Status {
  final String id;
  final String message;
  final String sender;
  int liker = 0;
  
  Status({
    required this.id,
    required this.message,
    required  this.sender,
  });

  void incLiker(int x){
    this.liker += x;
  }
}
