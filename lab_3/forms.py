from django import forms
from django import forms
from django.forms import fields
from lab_1.models import Friend

# class DateInput(forms.DateInput):
#     input_type = 'date'

# class FriendForm(forms.Form):
#     name = forms.CharField(max_length=30, label="Nama Kamu")
#     npm = forms.CharField(max_length=10, label="NPM Kamu")
#     birthdate= forms.DateField(label="Ulang Tahun Kamu", widget=forms.DateInput)

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'
        labels = {
            "name":"Masukan Nama Kamu",
            "npm":"Masukan NPM Kamu",
            "birthdate": "Masukan Tanggal Lahir Kamu"
        }
        error_messages = {
            "name":{"required":"Lengkapi Input",},
            "npm":{"required":"Lengkapi Input",},
            "birthdate":{
                "required":"Lengkapi Input",
                "invalid":"Format Tanggal Anda Salah"
            }
        }
    