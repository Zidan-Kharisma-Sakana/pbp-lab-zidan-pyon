from django.shortcuts import render
from django.http.response import Http404, HttpResponse, HttpResponseRedirect
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required

# Create your views here.
from .forms import FriendForm

def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'index_lab3.html', response)

@login_required(login_url="/admin/login/")
def addFriend(request):
    if(request.method == 'POST'):
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3')
    else:
        form = FriendForm()
    return render(request, 'add_friend_lab3.html', {
        "form": form
    })