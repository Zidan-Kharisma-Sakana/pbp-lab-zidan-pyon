from django.urls import path
from . import views

urlpatterns = [
    path('',views.index, name='index-lab3'),
    path('add-friend',views.addFriend, name='add-friend'),

]
