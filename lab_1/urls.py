from django.urls import path
from . import views

urlpatterns = [
    path('',views.index, name='index-lab1'),
    path('friend-list', views.friend_list, name='friend-list'),
    path('friend-list/<int:id>', views.friend_details, name="friend-detail")

]
