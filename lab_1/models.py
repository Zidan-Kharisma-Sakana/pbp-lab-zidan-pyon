from django.db import models
from django.urls.base import reverse

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=10)
    birthdate= models.DateField()

    def get_absolute_url(self):
        return reverse("friend-detail", args=[self.id])
    
    def __str__(self):
        return f"{self.name} - {self.npm} - {self.birthdate}"
