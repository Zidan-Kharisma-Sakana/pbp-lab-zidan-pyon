from django.http.response import Http404
from django.shortcuts import render
from datetime import datetime, date
from .models import Friend

mhs_name = 'Zidan Kharisma Adidarma' 
curr_year = int(datetime.now().strftime("%Y"))
birthdate = date(2002, 1, 22) 
npm = 2006463881


def index(request):
    response = {'name': mhs_name,
                'age': calculate_age(birthdate.year),
                'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)

def friend_details(request, id):
    try:
        teman = Friend.objects.get(id=id)
        response = {'name': teman.name,
                    'age': calculate_age(teman.birthdate.year),
                    'npm': teman.npm}
        return render(request, 'index_lab1.html', response)
    except:
        raise Http404()
