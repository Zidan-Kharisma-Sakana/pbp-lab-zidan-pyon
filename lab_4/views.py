from django.shortcuts import render
from django.http.response import Http404, HttpResponse, HttpResponseRedirect
from lab_2.models import Note
from django.contrib.auth.decorators import login_required

# Create your views here.
from .forms import NoteForm

def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'index_lab4.html', response)

def index2(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'card_lab4.html', response)

@login_required(login_url="/admin/login/")
def addFriend(request):
    if(request.method == 'POST'):
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-4')
    else:
        form = NoteForm()
    return render(request, 'add_note_lab4.html', {
        "form": form
    })