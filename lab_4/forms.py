from django import forms
from django import forms
from django.forms import fields, widgets
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = '__all__'
        widgets={
            'message': forms.Textarea()
        }
        labels = {
            "to":"Masukan Nama Penerima",
            "_from":"Masukan Nama Kamu",
            "title": "Masukan Judul Pesan",
            "message": "Masukan Pesan Kamu"
        }
    