from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=30)
  # from is reserved
    _from = models.CharField(max_length=30, db_column="from")
    title = models.CharField(max_length=30)
    message = models.CharField(max_length=200)
    
    def getFrom(self):
        return self._from

    def __str__(self):
        return f"From {self._from}, to {self.to} | {self.title}"
