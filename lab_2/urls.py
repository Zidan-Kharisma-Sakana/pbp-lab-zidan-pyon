from django.urls import path
from . import views

urlpatterns = [
    path('',views.index, name='index-lab2'),
    path('html/<int:id>', views.note_details , name='note-detail'),
    path('xml',views.xml_list, name='xml-lab2'),
    path('xml/<int:id>', views.xml_details , name='xml-lab2-detail'),
    path('json',views.json_list, name='json-lab2'),
    path('json/<int:id>', views.json_details , name='json-lab2-detail'),
]

