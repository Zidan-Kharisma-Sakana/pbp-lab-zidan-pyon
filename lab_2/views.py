from django.http.response import Http404, HttpResponse
from django.shortcuts import render
from datetime import datetime, date
from .models import Note
from django.core import serializers

# Create your views here.

def index(request):
    try:
        notes = Note.objects.all()
        response = {"notes": notes}
        return render(request, 'index_lab2.html', response)
    except:
        raise Http404()

def note_details(request,id):
    try:
        note = Note.objects.get(id=id)
        response = {"note": note}
        return render(request, 'details_lab2.html', response)
    except:
        raise Http404()

def xml_list(request):
    try:
        notes = Note.objects.all()
        data = serializers.serialize('xml', notes)
        return HttpResponse(data, content_type="application/xml")
    except:
        raise Http404()

def xml_details(request, id):
    try:
        note = Note.objects.get(id=id)
        data = serializers.serialize('xml', [note])
        return HttpResponse(data, content_type="application/xml")
    except:
        raise Http404()
def json_list(request):
    try:
        notes = Note.objects.all()
        data = serializers.serialize('json', notes)
        return HttpResponse(data, content_type="application/json")
    except:
        raise Http404()

def json_details(request, id):
    try:
        note = Note.objects.get(id=id)
        data = serializers.serialize('json', [note])
        return HttpResponse(data, content_type="application/json")
    except:
        raise Http404()
